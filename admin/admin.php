<?php
//Admin panel
session_start();

require_once '../db_config.php';
require_once '../functions_def.php';

global $pdo;

//check admin role for user
if (!isset($_SESSION['email']) or !isset($_SESSION['id']) or !is_int($_SESSION['id']) or !isset($_SESSION['role']) or $_SESSION['role'] != 'admin') {
    redirection('../index.php?l=0');
}
$select = $_GET['select'] ?? '';
$_SESSION['select'] = $select;

setcookie('select', $select, time() + (86400 * 30), "/");
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1" id="metaViewport" name="viewport">
    <meta charset="UTF-8">
    <meta name="author" content="Porpoises">
    <meta name="description" content="">
    <meta name="robots" content="index, follow">
    <title>Admin</title>
    <link rel="stylesheet" href="admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../favicon.ico">
</head>
<body>
<?php $select = $_GET['select'] ?? ''; ?>
<button class="btn" type="button" onclick="goBack();">Back to the Page</button>
<a href="admin.php?op=new&table=<?= $select ?>" class="btn">Add New Item</a>
<a href="../logout.php" class="btn">Logout</a>
<span class='info'><?php echo 'Logged in as ' . $_SESSION['email']; ?></span>
<table id='selects'>
    <tr>
        <td><a href="admin.php?select=breed"<?php if ($select == 'breed') echo "style='font-weight: bold;'" ?>>SELECT
                breed</a></td>
    </tr>
    <tr>
        <td>
            <a href="admin.php?select=doctor_service"<?php if ($select == 'doctor_service') echo "style='font-weight: bold;'" ?>>SELECT
                doctor_service</a></td>
    </tr>
    <tr>
        <td>
            <a href="admin.php?select=medicalcards"<?php if ($select == 'medicalcards') echo "style='font-weight: bold;'" ?>>SELECT
                medicalcards</a></td>
    </tr>
    <tr>
        <td><a href="admin.php?select=pets"<?php if ($select == 'pets') echo "style='font-weight: bold;'" ?>>SELECT
                pets</a></td>
    </tr>
    <tr>
        <td>
            <a href="admin.php?select=reservations"<?php if ($select == 'reservations') echo "style='font-weight: bold;'" ?>>SELECT
                reservations</a></td>
    </tr>
    <tr>
        <td><a href="admin.php?select=schedules"<?php if ($select == 'schedules') echo "style='font-weight: bold;'" ?>>SELECT
                schedules</a></td>
    </tr>
    <tr>
        <td><a href="admin.php?select=services"<?php if ($select == 'services') echo "style='font-weight: bold;'" ?>>SELECT
                services</a></td>
    </tr>
    <tr>
        <td><a href="admin.php?select=users"<?php if ($select == 'users') echo "style='font-weight: bold;'" ?>>SELECT
                users</a></td>
    </tr>
</table>

<div id="container" style="width:70vw; overflow:auto; margin: auto; ">

    <?php

    $select = $_GET['select'] ?? '';

    if ($select == 'breed') {
        $sql = "SELECT * FROM breed";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";
        echo "<tr>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['name']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=breed' class='update'>Update</a></td><td><a href='admin.php?select=breed&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }
        echo '</table>';
    }

    if ($select == 'doctor_service') {
        $sql = "SELECT * FROM doctor_service";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['user_id']}</td><td>{$row['service_id']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=doctor_service' class='update'>Update</a></td><td><a href='admin.php?select=doctor_service&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }

        echo '</table>';
    }

    if ($select == 'medicalcards') {
        $sql = "SELECT * FROM medicalcards";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['pet_id']}</td><td>{$row['doc_id']}</td><td>{$row['service_id']}</td><td>{$row['date']}</td></td><td>{$row['description']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=medicalcards' class='update'>Update</a></td><td><a href='admin.php?select=medicalcards&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }

        echo '</table>';
    }

    if ($select == 'pets') {
        $sql = "SELECT * FROM pets";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['user_id']}</td><td>{$row['breed_id']}</td><td>{$row['name']}</td><td>{$row['birth']}</td><td>{$row['image']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=pets' class='update'>Update</a></td><td><a href='admin.php?select=pets&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }

        echo '</table>';
    }

    if ($select == 'reservations') {
        $sql = "SELECT id, user_id, doc_id, pets_id, date, hour, is_urgent, service_id FROM reservations";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['user_id']}</td><td>{$row['doc_id']}</td><td>{$row['pets_id']}</td><td>{$row['date']}</td><td>{$row['hour']}</td><td>{$row['is_urgent']}</td><td>{$row['service_id']}</td><td><a href='rows.php?op=update&id={$row['id']}&op=update&select=reservations' class='update'>Update</a></td><td><a href='admin.php?select=reservations&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }

        echo '</table>';
    }

    if ($select == 'schedules') {
        $sql = "SELECT * FROM schedules";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['doc_id']}</td><td>{$row['work_date']}</td><td>{$row['start_workhour']}</td><td>{$row['end_workhour']}</td><td>{$row['is_holiday']}</td><td>{$row['is_weekend']}</td><td>{$row['is_taken']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=schedules' class='update'>Update</a></td><td><a href='admin.php?select=schedules&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }

        echo '</table>';
    }

    if ($select == 'services') {
        $sql = "SELECT * FROM services";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['name']}</td><td>{$row['description']}</td><td>{$row['price']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=services' class='update'>Update</a></td><td><a href='admin.php?select=services&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></td></tr>";
        }

        echo '</table>';
    }

    if ($select == 'users') {
        $sql = "SELECT id, firstname, lastname, status, image, phone, email, department, penalty, active FROM users";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        echo "<table id='selected_user'>";

        foreach ($results[0] as $key => $value) {

            echo "<th>$key</th>";
        }

        echo "</tr>";

        foreach ($results as $row) {

            echo "<tr><td>{$row['id']}</td><td>{$row['firstname']}</td><td>{$row['lastname']}</td><td>{$row['status']}</td><td><img src='{$row['image']}' alt='{$row['image']}' class='user_img'></td><td>{$row['phone']}</td><td>{$row['email']}</td><td>{$row['department']}</td><td>{$row['penalty']}</td><td>{$row['active']}</td><td><a href='rows.php?id={$row['id']}&op=update&select=users' class='update'>Update</a></td><td><a href='admin.php?select=users&op=delete&id={$row['id']}' onclick='return confirm(\"Are you sure?\");' class='delete'>Delete</a></td></tr>";
        }

        echo '</table>';
    }
    ?>
</div>

<center><img src="../images/loading_icon.gif" alt="Loading..." id="loading" class="hidden"></center>
<form action="admin.php" method="GET" class="hidden" id="form1" name="form1">
    <table id="inputs">
        <?php

        if ($select == 'breed') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>Name: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'doctor_service') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>User_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Service_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'medicalcards') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>Pet_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Doc_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Service_ID: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Date: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Description: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'pets') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>User_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Breed_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Name: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Birth: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Image: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'reservations') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>User_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Doc_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Date: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Is_Urgent: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Service_ID: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'schedules') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>Doc_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Work_Date: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Start_Workhour: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>End_Workhour: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Is_Holiday: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <tr><td><label for='text7'>Is_Weekend: </label></td><td><input type='text' id='text7' name='text7'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'services') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>Name: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Description: </label></td><td><textarea name='text3' id='text3' cols='21' rows='10'></textarea></td></tr>
            <tr><td><label for='text4'>Price: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }

        if ($select == 'users') {

            echo "
            <tr><td><label for='text1'>ID: </label></td><td><input type='text' id='text1' name='text1' readonly></td></tr>
            <tr><td><label for='text2'>Pets_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Worktime_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>First Name: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Last Name: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Status: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <tr><td><label for='text7'>Image: </label></td><td><input type='text' id='text7' name='text7'></td></tr>
            <tr><td><label for='text8'>Phone: </label></td><td><input type='text' id='text8' name='text8'></td></tr>
            <tr><td><label for='text9'>Email: </label></td><td><input type='text' id='text9' name='text9'></td></tr>
            <tr><td><label for='text10'>Department: </label></td><td><input type='text' id='text10' name='text10'></td></tr>
            <tr><td><label for='text11'>Penalty: </label></td><td><input type='text' id='text11' name='text11'></td></tr>
            <tr><td><label for='text12'>Active: </label></td><td><input type='text' id='text12' name='text12'></td></tr>
            <input type='hidden' name='operation' value='update'>
            ";
        }
        ?>
    </table>
    <input type="submit" value="Update" id="updButton">
</form>

<?php
$id = $_GET['id'] ?? '';
$op = $_GET['op'] ?? '';
$table = $_GET['table'] ?? '';
$text1 = $_GET['text1'] ?? '';
$text2 = !empty($_GET['text2']) ? $_GET['text2'] : null;
$text3 = !empty($_GET['text3']) ? $_GET['text3'] : null;
$text4 = !empty($_GET['text4']) ? $_GET['text4'] : null;
$text5 = !empty($_GET['text5']) ? $_GET['text5'] : null;
$text6 = !empty($_GET['text6']) ? $_GET['text6'] : null;
$text7 = !empty($_GET['text7']) ? $_GET['text7'] : null;
$text8 = !empty($_GET['text8']) ? $_GET['text8'] : null;
$text9 = !empty($_GET['text9']) ? $_GET['text9'] : null;
$text10 = !empty($_GET['text10']) ? $_GET['text10'] : null;
$text11 = !empty($_GET['text11']) ? $_GET['text11'] : null;
$text12 = !empty($_GET['text12']) ? $_GET['text12'] : null;

if ($op == 'delete') {
    $sql = "DELETE FROM $select WHERE id = $id";
    $query = $pdo->prepare($sql);

    if ($query->execute()) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Item deleted successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Delete failed!');
            }, 100); 
            </script>";
    }
}

if ($op == 'new' && $table == 'breed') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>Name: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='breed'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'doctor_service') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>User_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Service_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='doctor_service'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'medicalcards') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>Pet_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Doc_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Service_ID: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Date: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Description: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='medicalcards'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'pets') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>User_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Breed_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Name: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Birth: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Image: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='pets'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'reservations') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>User_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Doc_ID: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Date: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Is_Urgent: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Service_ID: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='reservations'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'schedules') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>Doc_ID: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Work_Date: </label></td><td><input type='text' id='text3' name='text3'></td></tr>
            <tr><td><label for='text4'>Start_Workhour: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>End_Workhour: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Is_Holiday: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <tr><td><label for='text7'>Is_Weekend: </label></td><td><input type='text' id='text7' name='text7'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='schedules'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'services') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text2'>Name: </label></td><td><input type='text' id='text2' name='text2'></td></tr>
            <tr><td><label for='text3'>Description: </label></td><td><textarea name='text3' id='text3' cols='21' rows='10'></textarea></td></tr>
            <tr><td><label for='text4'>Price: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='services'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}

if ($op == 'new' && $table == 'users') {

    echo "<form action='admin.php' method='GET' id='form2' name='form2'>
        <table id='inputs'>";

    echo "
            <tr><td><label for='text4'>First Name: </label></td><td><input type='text' id='text4' name='text4'></td></tr>
            <tr><td><label for='text5'>Last Name: </label></td><td><input type='text' id='text5' name='text5'></td></tr>
            <tr><td><label for='text6'>Status: </label></td><td><input type='text' id='text6' name='text6'></td></tr>
            <tr><td><label for='text7'>Image: </label></td><td><input type='text' id='text7' name='text7'></td></tr>
            <tr><td><label for='text8'>Phone: </label></td><td><input type='text' id='text8' name='text8'></td></tr>
            <tr><td><label for='text9'>Email: </label></td><td><input type='text' id='text9' name='text9'></td></tr>
            <tr><td><label for='text10'>Department: </label></td><td><input type='text' id='text10' name='text10'></td></tr>
            <tr><td><label for='text12'>Active: </label></td><td><input type='text' id='text12' name='text12'></td></tr>
            <input type='hidden' name='operation' value='insert'>
            <input type='hidden' name='table' value='users'>
            ";

    echo "</table><input type='submit' value='Insert' id='insButton'></form>";
}
/************************* INSERT NEW *************************/

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'breed') {
    $sql = "INSERT INTO breed (name) VALUES (?)";

    if ($pdo->prepare($sql)->execute([$text2])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'doctor_service') {
    $sql = "INSERT INTO doctor_service (user_id, service_id) VALUES (?,?)";

    if ($pdo->prepare($sql)->execute([$text2, $text3])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'medicalcards') {
    $sql = "INSERT INTO medicalcards (pet_id, doc_id, service_id, date, description) VALUES (?,?,?,?,?)";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'pets') {
    $sql = "INSERT INTO pets (user_id, breed_id, name, birth, image) VALUES (?,?,?,?,?)";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'reservations') {
    $sql = "INSERT INTO reservations (user_id, doc_id, date, is_urgent, service_id) VALUES (?,?,?,?,?)";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'schedules') {
    $sql = "INSERT INTO schedules (doc_id, work_date, start_workhour, end_workhour, is_holiday, is_weekend) VALUES (?,?,?,?,?,?)";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6, $text7])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'services') {
    $sql = "INSERT INTO services (name, description, price) VALUES (?,?,?)";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'insert' && $_GET['table'] == 'users') {

    if (filter_var($text9, FILTER_VALIDATE_EMAIL)) {
        $sql = "INSERT INTO users (firstname, lastname, status, image, phone, email, department, active) VALUES (?,?,?,?,?,?,?,?)";

        if ($pdo->prepare($sql)->execute([$text4, $text5, $text6, $text7, $text8, $text9, $text10, $text12])) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('New item added successfully!');
            }, 100); 
            </script>";
        } else {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
        }
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Invalid email!');
            }, 100); 
            </script>";
    }
}

$text1 = $_GET['text1'] ?? '';
$text2 = !empty($_GET['text2']) ? $_GET['text2'] : null;
$text3 = !empty($_GET['text3']) ? $_GET['text3'] : null;
$text4 = !empty($_GET['text4']) ? $_GET['text4'] : null;
$text5 = !empty($_GET['text5']) ? $_GET['text5'] : null;
$text6 = !empty($_GET['text6']) ? $_GET['text6'] : null;
$text7 = !empty($_GET['text7']) ? $_GET['text7'] : null;
$text8 = !empty($_GET['text8']) ? $_GET['text8'] : null;
$text9 = !empty($_GET['text9']) ? $_GET['text9'] : null;
$text10 = !empty($_GET['text10']) ? $_GET['text10'] : null;
$text11 = !empty($_GET['text11']) ? $_GET['text11'] : null;
$text12 = !empty($_GET['text12']) ? $_GET['text12'] : null;

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'breed') {
    $sql = "UPDATE breed SET 
        name=?
	    WHERE id=?";

    //Update successful alert appear
    if ($pdo->prepare($sql)->execute([$text2, $text1])) {

        echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Successfully updated!');
                    }, 100); 
                  </script>";
    } else {

        echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Update failed');
                    }, 100); 
                  </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'doctor_service') {
    $sql = "UPDATE doctor_service SET 
        user_id=?,
        service_id=?
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text1])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
            </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Update failed');
            }, 100); 
          </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'medicalcards') {
    $sql = "UPDATE medicalcards SET 
        pet_id=?,
        doc_id=?,
        service_id=?,
        date=?,
        description=?
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6, $text1])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
          </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Update failed');
            }, 100); 
          </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'pets') {
    $sql = "UPDATE pets SET 
        user_id=?,
        breed_id=?,
        name=?,
        birth=?,
        image=?s
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6, $text1])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
          </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Update failed');
            }, 100); 
          </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'reservations') {
    $sql = "UPDATE reservations SET 
        user_id=?,
        doc_id=?,
        date=?,
        is_urgent=?,
        service_id=?
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6, $text1])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
          </script>";
    } else {

        echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Update failed');
                    }, 100); 
                  </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'schedules') {
    $sql = "UPDATE schedules SET 
        doc_id=?,
        work_date=?,
        start_workhour=?,
        end_workhour=?,
        is_holiday=?,
        is_weekend=?
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6, $text7, $text1])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
          </script>";
    } else {

        echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Update failed');
                    }, 100); 
                  </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'services') {
    $sql = "UPDATE services SET 
        name=?,
        description=?,
        price=?
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text1])) {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Successfully updated!');
            }, 100); 
          </script>";
    } else {

        echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Update failed');
            }, 100); 
          </script>";
    }
}

if (isset($_GET['operation']) && $_GET['operation'] == 'update' && $_COOKIE['select'] == 'users') {
    $sql = "UPDATE users SET 
        pets_id=?, 
        worktime_id=?,
        firstname=?,
        lastname=?,
        status=?,
        image=?,
        phone=?,
        email=?,
        department=?,
        penalty=?,
        active=?
	    WHERE id=?";

    if ($pdo->prepare($sql)->execute([$text2, $text3, $text4, $text5, $text6, $text7, $text8, $text9, $text10, $text11, $text12, $text1])) {

        echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Successfully updated!');
                    }, 100); 
                  </script>";
    } else {

        echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Update failed');
                    }, 100); 
                  </script>";
    }
}
?>
<script>
    var loading_icon = document.getElementById('loading');
    for (let item of document.querySelectorAll('a.update')) {
        item.onclick = function (e) {
            form1.classList.add('hidden');
            loading_icon.classList.remove('hidden');
            e.preventDefault();
            let url = this.href;
            let data;
            let splits = (url.split('='));
            let sel = splits[3];
            fetch(url)
            .then(response => response.json())
            .then(json => {

                form1.classList.remove('hidden');
                loading_icon.classList.add('hidden');

                if (sel == 'breed') {
                    text1.value = json.id;
                    text2.value = json.name;
                }

                if (sel == 'doctor_service') {
                    text1.value = json.id;
                    text2.value = json.user_id;
                    text3.value = json.service_id;
                }

                if (sel == 'medicalcards') {
                    text1.value = json.id;
                    text2.value = json.pet_id;
                    text3.value = json.doc_id;
                    text4.value = json.service_id;
                    text5.value = json.date;
                    text6.value = json.description;
                }

                if (sel == 'pets') {
                    text1.value = json.id;
                    text2.value = json.user_id;
                    text3.value = json.breed_id;
                    text4.value = json.name;
                    text5.value = json.birth;
                    text6.value = json.image;
                }

                if (sel == 'reservations') {
                    text1.value = json.id;
                    text2.value = json.user_id;
                    text3.value = json.doc_id;
                    text4.value = json.date;
                    text5.value = json.is_urgent;
                    text6.value = json.service_id;
                }

                if (sel == 'schedules') {
                    text1.value = json.id;
                    text2.value = json.doc_id;
                    text3.value = json.work_date;
                    text4.value = json.start_workhour;
                    text5.value = json.end_workhour;
                    text6.value = json.is_holiday;
                    text7.value = json.is_weekend;
                }

                if (sel == 'services') {
                    text1.value = json.id;
                    text2.value = json.name;
                    text3.value = json.description;
                    text4.value = json.price;
                }

                if (sel == 'users') {
                    text1.value = json.id;
                    text4.value = json.firstname;
                    text5.value = json.lastname;
                    text6.value = json.status;
                    text7.value = json.image;
                    text8.value = json.phone;
                    text9.value = json.email;
                    text10.value = json.department;
                    text11.value = json.penalty;
                    text12.value = json.active;
                }
            });
        }
    }
</script>
<script src="admin.js"></script>
</body>
</html>