<?php
//Log in panel on administrator page

//If user is here we clear session data (log out the user)
session_start();
$_SESSION = array();

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1" id="metaViewport" name="viewport">
    <meta charset="UTF-8">
    <meta name="author" content="Porpoises">
    <meta name="description" content="">
    <meta name="robots" content="index, follow">
    <title>Admin</title>
    <link rel="stylesheet" href="admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../favicon.ico">
    <style>
    
    div:first-of-type {
        background-color: #000000;
        background-image: linear-gradient(315deg, #000000 0%, #414141 74%);
        display: grid;
        height: 100vh;
        color: white;
        align-items: center;
        text-align: center;
    }
    table {
        margin: auto;
    }
    button {
        background-color: #c2fbd7;
        border-radius: 100px;
        box-shadow: rgba(44, 187, 99, .2) 0 -25px 18px -14px inset,rgba(44, 187, 99, .15) 0 1px 2px,rgba(44, 187, 99, .15) 0 2px 4px,rgba(44, 187, 99, .15) 0 4px 8px,rgba(44, 187, 99, .15) 0 8px 16px,rgba(44, 187, 99, .15) 0 16px 32px;
        color: green;
        cursor: pointer;
        display: inline-block;
        font-family: CerebriSans-Regular,-apple-system,system-ui,Roboto,sans-serif;
        padding: 7px 20px;
        text-align: center;
        text-decoration: none;
        transition: all 250ms;
        border: 0;
        font-size: 16px;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
        margin: 1rem 0.5rem;
    }
    td {
        text-align: right;
    }
    a{
        color: white;
    }
    button:hover {
        background-color: white;
        color: black;
    }
    input:focus {
        outline: none !important;
        border-color: #719ECE;
        box-shadow: 0 0 10px #719ECE;
    }
    tr:last-child {
        border-spacing: 0 1rem;
    }
    td a {
        font-size: 80%;
    }
    button:hover {
        box-shadow: rgba(44,187,99,.35) 0 -25px 18px -14px inset,rgba(44,187,99,.25) 0 1px 2px,rgba(44,187,99,.25) 0 2px 4px,rgba(44,187,99,.25) 0 4px 8px,rgba(44,187,99,.25) 0 8px 16px,rgba(44,187,99,.25) 0 16px 32px;
        transform: scale(1.05) rotate(-1deg);
    }
    a:hover {
        color: #90EE90;
    }
    .invisible {
        display: none;
    }
    </style>
</head>
<body>
    <div>
        <form action="check_login.php" method="POST">
            <table id="login">
                <tr>
                    <td><label for="email">Email</label></td><td><input type="email" name="email" id="email"></td>
                </tr>
                <tr id="trPass">
                    <td><label for="password">Password</label></td><td><input type="password" name="password" id="password"></td>
                </tr>
            </table>

            <button id="backBt" type="button" onclick="goBack();">Back to the Page</button>
            <button id="loginBt" type="submit" onclick="validate(email, event)">Login</button>
        </form>
    </div>
    <script src="admin.js"></script>
    <?php
    require_once "../db_config.php";
    global $messages;

    $l = 0;

    if (isset($_GET["l"]) and is_numeric($_GET['l'])) {
        $l = (int)$_GET["l"];

        if (array_key_exists($l, $messages)) {
            echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('$messages[$l]');
                    }, 100); 
                  </script>";
        }
    }
    ?>
<script>
    function $(id) {
        return document.getElementById(id);
    }
    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    function validate(email, event) {
        let password = $('password');
        if (email.value.match(validRegex))
            if (password.value.length < 8) {
                alert("The password has to be at least 8 characters long!");
                event.preventDefault();
            }
            else
                return true;
        else {
           alert("Invalid email address!");
           event.preventDefault();
        }
    }
    
</script>
</body>
</html>