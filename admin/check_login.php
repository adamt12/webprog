<?php
//There is check for log in on admin page

session_start();
require_once "../db_config.php";
require "../functions_def.php";

global $pdo;

$referer = $_SERVER['HTTP_REFERER'];
$email = mb_substr($pdo->quote(trim($_POST['email'])), 1, -1);
$password = mb_substr($pdo->quote(trim($_POST['password'])), 1, -1);

if (strpos($referer, SITE . "admin/") !== false) {

    if (!empty($email) and !empty($password)) {
        $data = checkUserLogin($email, $password, "admin");

        if ($data and is_int($data['id'])) {
            // session_regenerate_id();
            $_SESSION['email'] = $email;
            $_SESSION['id'] = $data['id'];
            $_SESSION['role'] = $data['role'];
            redirection('admin.php');
        } else {
            redirection('index.php?l=1');
        }

    } else {
        redirection('index.php?l=1');
    }
}else{
    redirection('index.php?l=0'. $referer . "?" . SITE . "admin/");
}
?>