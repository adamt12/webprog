<?php
//Creating json for store data, admin can update or delete these data on admin panel
require_once '../db_config.php';

global $pdo;

$select = $_GET['select'] ?? '';
$id = $_GET['id'] ?? '';

//users and reservations are special tables, need specialized query, on other tables we select all of the data
if ($select == 'users') 
    $sql = "SELECT id, pets_id, worktime_id, firstname, lastname, status, image, phone, email, department, penalty, active FROM users WHERE id = $id";
if ($select == 'reservations')
    $sql = "SELECT id, date, hour FROM reservations WHERE id = $id";
else
    $sql = "SELECT * FROM $select WHERE id = $id";

$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll(PDO::FETCH_ASSOC);


echo json_encode($results[0]);