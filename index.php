<?php
//main page

session_start();

require_once "functions_def.php";

global $pdo;

$loginState = false;
$now = $_SERVER['REQUEST_TIME'];

if (!isset($_SESSION['email']) OR !isset($_SESSION['id']) OR !is_int($_SESSION['id']) OR !isset($_SESSION['role'])) {
    $loginState = false;
} else {
    $loginState = true;
}

//Delete users from database with expired registration
$expUserId = "";
$sql = "SELECT id FROM users WHERE active=0 AND registration_expires < now()";
$query = $pdo->prepare($sql);
$query->execute();
$results_expired_regs = $query->fetchAll(PDO::FETCH_ASSOC);

foreach ($results_expired_regs as $row) {
    $expUserId = $row['id'];

    $sql = "DELETE FROM users WHERE id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $expUserId, PDO::PARAM_INT);
    $stmt->execute();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1" id="metaViewport" name="viewport">
    <meta charset="UTF-8">
    <meta name="author" content="Porpoises">
    <meta name="description" content="">
    <meta name="robots" content="index, follow">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <?php 

    if (!$loginState) {

        echo "<style>
        #login::before { 
            width: 0 !important; 
        }
        </style>";
    }

    if ($loginState) {

        echo "<style>
        #login {
            display: flex !important;
            align-items: center !important;
            justify-content: center;
            
        }
        #login::before {
            top: 3.23rem;
        }
        </style>";
    }

    if (isset($_GET['op'])) {
        $op = $_GET['op'];

        if ($op == 'form' || $op == 'profile' || $op =='doc') {

            echo '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">';
        }

        if ($op == 'main') {

            echo "<style>
            body {
                background-image: url(images/bg.jpg); 
                background-size: cover; 
                background-repeat:no-repeat;
            }
            @media (max-width: 1000px) {
                body {
                    background-image: url(images/bg-phone.jpg);
                    background-color: #F7F7F7;
                }
            }
            </style>";
        }
    } else {

        echo "<style>
        body {
            background-image: url(images/bg.jpg); 
            background-size: cover; 
            background-repeat:no-repeat;
        }
        @media (max-width: 1000px) {
            body {
                background-image: url(images/bg-phone.jpg);
                background-color: #F7F7F7;
            }
        }
        </style>";
        $op = 'main';
    }
    ?>

    <style>
        .noscript {
            background: red;
            color: #fff;
            padding: 10px;
            text-align: center;
            position: relative;
            z-index: 3;
        }
    </style>
    <title><?=ucfirst($op);?></title>
</head>
<body>
<?php //check is javascript enabled ?>
<noscript>
    <div class="noscript">
        JavaScript must be enabled in order for you to use the WEBSITE in standard view.<br />
        However, it seems JavaScript is either disabled or not supported by your browser.<br />
        To use standard view, enable JavaScript by changing your browser options, then <a href="index.php">try again</a>.
    </div>
</noscript>

<?php
//import navigation
require_once "navigation.php";
?>

<div id="container">
    <?php

    if (isset($_GET['op'])) {
        $op = $_GET['op'];
        $pages = ['main','doctors','contact', 'karton', 'animal', 'form', 'profile', 'doc'];

        if (in_array($op, $pages)) {
            include_once ($op . '.php');
        } else {
            include_once 'index.php';
        }
    } else {
        $op = 'main';
        include_once($op . '.php');
    }

    $id=$_SESSION['id'] ?? '';

    if (isset($_POST['operation']) && $_POST['operation'] == 'update') {
        $firstname = $_POST['firstname'] ?? '';
        $lastname = $_POST['lastname'] ?? '';
        $picture = $_POST['picture'] ?? '';
        $email = $_POST['email'] ?? '';
        $password = $_POST['password'] ?? '';
        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = "UPDATE users SET 
                firstname=?,
                lastname=?,
                image=?,
                email=?,
                password=?
                WHERE id=?";

        if($pdo->prepare($sql)->execute([$firstname, $lastname, $picture, $email, $password, $id])) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Profile updated successfully!');
            }, 100); 
            </script>";
        }else{

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Update failed!');
            }, 100); 
            </script>";
        }
    }

    if (isset($_POST['operation']) && $_POST['operation'] == 'insert') {
        $breed = $_POST['breed'] ?? '';
        $name = $_POST['petname'] ?? '';
        $birth = $_POST['petbirth'] ?? '';
        $image = $_POST['petPicture'] ?? '';


        $sql = "INSERT INTO pets (user_id, breed_id, name, birth, image) VALUES (?,?,?,?,?)";

        if($pdo->prepare($sql)->execute([$id, $breed, $name, $birth, $image])) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Pet inserted successfully!');
            }, 100); 
            </script>";
        }else{

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
        }
    }

    if (isset($_POST['operation']) && $_POST['operation'] == 'insertSchedule') {
        $id = $_POST['id'] ?? '';
        $date = $_POST['workdate'] ?? '';
        $starthour = $_POST['starthour'] ?? '';
        $endhour = $_POST['endhour'] ?? '';
        $holiday = $_POST['holiday'] ?? '';
        $weekend = $_POST['weekend'] ?? '';

        $sql = "INSERT INTO schedules (doc_id, work_date, start_workhour, end_workhour, is_holiday, is_weekend) VALUES (?,?,?,?,?,?)";

        if($pdo->prepare($sql)->execute([$id, $date, $starthour, $endhour, $holiday, $weekend])) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Schedule inserted successfully!');
            }, 100); 
            </script>";
        }else{

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Insert failed!');
            }, 100); 
            </script>";
        }
    }

    if (isset($_GET['operation']) && $_GET['operation'] == 'insertAppointment') {

        if ($_SESSION['role'] == 'doctor') {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Doctors can not make appointments!');
            }, 100); 
            </script>";
        } else {
            $user_id = $_SESSION['id'] ?? '';
            $doc_id = $_GET['doc_id'] ?? '';
            $pets_id = $_GET['pets_id'] ?? '';
            $date = $_GET['date'] ?? '';
            $hour = $_GET['hour'] ?? '';
            $is_urgent = $_GET['urgent'] ?? '';
            $service_id = $_GET['service'] ?? '';

            $datetime = "$date $hour:00";
            $selected_date = strtotime($datetime);

            if ($selected_date > strtotime('+1 month', $now)) {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Insert failed! Selected date can not be in more than 1 month.');
                }, 100); 
                </script>";
            } else {
                $fail = 1;
                $sql = "SELECT * FROM schedules WHERE doc_id = $doc_id";
                $query = $pdo->prepare($sql);
                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_ASSOC);

                foreach ($results as $row) {
                    $workstart = strtotime("{$row['work_date']} {$row['start_workhour']}:00");
                    $workend = strtotime("{$row['work_date']} {$row['end_workhour']}:00");

                    if (($selected_date >= $workstart && $selected_date < $workend) && ($row['is_holiday'] == 0 && $row['is_weekend'] == 0 && $row['is_taken'] == 0)) {
                        $fail = 0;
                    }
                }

                if ($fail == 0) {
                    $codeForReservation = generateToken();
                    $sql = "INSERT INTO reservations (user_id, doc_id, pets_id, date, hour, is_urgent, service_id, token) VALUES (?,?,?,?,?,?,?,?)";

                    if($pdo->prepare($sql)->execute([$user_id, $doc_id, $pets_id, $date, $hour, $is_urgent, $service_id, $codeForReservation])) {

                        echo "<script type='text/javascript'>
                        window.setTimeout(function(){
                            alert('Reservation inserted successfully!');
                        }, 100); 
                        </script>";

                        $sql = "UPDATE schedules SET is_taken=1 WHERE start_workhour=? AND work_date=?";
                        $pdo->prepare($sql)->execute([$hour, $date]);


                        $sql = "SELECT email FROM users where id = $user_id";
                        $query = $pdo->prepare($sql);
                        $query->execute();
                        $results = $query->fetch(PDO::FETCH_ASSOC);

                        $userEmail = $results['email'];
                        sendDataReservation($userEmail, $codeForReservation, $date, $hour);
                    }else{

                        echo "<script type='text/javascript'>
                        window.setTimeout(function(){
                            alert('Insert failed!');
                        }, 100); 
                        </script>";
                    }
                }
                
                else {
                    echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('Error! The doctor is not available at the selected date.');
                    }, 100); 
                    </script>";
                }
            }
        }
    }

    if (isset($_GET['operation']) && $_GET['operation'] == 'penalty') {
        $user_id = $_GET['user_id'] ?? '';

        $sql = "UPDATE users SET penalty = penalty + 1 WHERE id = ?";

        if(!$pdo->prepare($sql)->execute([$user_id])) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Error!');
            }, 100); 
            </script>";
        }


        $sql = "SELECT penalty FROM users WHERE id = $user_id";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        if ($results[0]['penalty'] == 3) {
            $sql = "UPDATE users SET active = 0 WHERE id = ?";
            $pdo->prepare($sql)->execute([$user_id]);

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('This user is successfully banned!');
            }, 100); 
            </script>";
        } else if ($results[0]['penalty'] > 3) {
            $sql = "UPDATE users SET active = 0, penalty = 3 WHERE id = ?";
            $pdo->prepare($sql)->execute([$user_id]);

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('This user is already banned!');
            }, 100); 
            </script>";
        } else if ($results[0]['penalty'] < 3 && $results[0]['penalty'] >= 0) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Penalty added!');
            }, 100); 
            </script>";
        }
    }

    if (isset($_GET['operation']) && $_GET['operation'] == 'reservation_del') {
        $reservation_id = $_GET['reservation_id'];
        $date = $_GET['date'];
        $hour = $_GET['hour'];
        $datetime = "$date $hour:00";
        $selected_date = strtotime($datetime);
        
        if ($selected_date < strtotime('+4 hours', $now)) {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert(\"Can not delete reservation if it's in less than 4 hours!\");
            }, 100); 
            </script>";
        } else {
            $sql = "DELETE FROM reservations WHERE id = $reservation_id";
            $query = $pdo->prepare($sql);

            if($query->execute()){

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Reservation deleted successfully!');
                }, 100); 
                </script>";
            } else {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Error!');
                }, 100); 
                </script>";
            }
        }
    }

    if (isset($_GET['operation']) && $_GET['operation'] == 'reservation_upd') {
        $reservation_id = $_GET['id'];
        $date = $_GET['date1'];
        $hour = $_GET['hour1'];
        $datetime = "$date $hour:00";
        $selected_date = strtotime($datetime);
        
        if ($selected_date > $now && $selected_date < strtotime('+1 month', $now) && $selected_date > strtotime('+4 hours', $now)) {
            $sql = "UPDATE reservations SET date = '$date', hour = $hour WHERE id = $reservation_id";

            if ($pdo->prepare($sql)->execute()) {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Reservation changed successfully!');
                }, 100); 
                </script>";
            } else {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Update failed!');
                }, 100); 
                </script>";
            }  
        } else {

            echo "<script type='text/javascript'>
            window.setTimeout(function(){
                alert('Update failed!');
            }, 100); 
            </script>";
        }  
    }

    if (isset($_GET['animal_modify']) || isset($_GET['animal_delete'])) {
        $pet_id = $_GET['pet_id'];
        $name = $_GET['name'] ?? null;
        $birth = $_GET['birth'] ?? null;
        $image = $_GET['picture'] ?? null;

        if (isset($_GET['animal_modify'])) {
            $sql = "UPDATE pets SET name = '$name', birth = $birth, image = '$image' WHERE id = $pet_id";

            if ($pdo->prepare($sql)->execute()) {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Pet details successfully changed!');
                }, 100); 
                </script>";
            } else {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Update failed!');
                }, 100); 
                </script>";
            } 

        }

        if (isset($_GET['animal_delete'])) {
            $sql = "DELETE FROM pets WHERE id = $pet_id";
            $query = $pdo->prepare($sql);

            if($query->execute()){

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Pet successfully deleted!');
                }, 100); 
                </script>";
            } else {

                echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('Delete failed!');
                }, 100); 
                </script>";
            }
        }
    }
?>

</div>

<?php require_once "footer.php"; ?>
    <?php

    require_once "db_config.php";

    global $messages;

    $l = 0; $r = 0;

    if (isset($_GET["l"]) and is_numeric($_GET['l'])) {
        $l = (int)$_GET["l"];

        if (array_key_exists($l, $messages)) {

            echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('$messages[$l]');
                    }, 100); 
                  </script>";
        }
    }

    if (isset($_GET["r"]) and is_numeric($_GET['r'])) {
        $r = (int)$_GET["r"];

        if (array_key_exists($r, $messages)) {

            echo "<script type='text/javascript'>
                    window.setTimeout(function(){
                        alert('$messages[$r]');
                    }, 100); 
                  </script>";
        }
    }
    ?>
</body>
</html>