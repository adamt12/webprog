<?php
//Insert new operation

session_start();

require_once "db_config.php";
require_once "medicalcards.php";
require "functions_def.php";

global $pdo;

$referer = $_SERVER['HTTP_REFERER'];
$petId = ""; $doctorId = ""; $serviceId = ""; $date = ""; $price = ""; $description = "";

//check access for this site
if(strpos($referer, SITE) === false OR !isset($_POST['enable']) OR !isset($_POST['doctor_id']) OR !isset($_POST['pet_id'])){
    redirection('index.php?r=0');
}

//check all required field is set
if(!isset($_POST['animal']) OR !isset($_POST['doctor']) OR !isset($_POST['date']) OR !isset($_POST['service']) OR !isset($_POST['price'])){
    redirection('index.php?op=karton&r=4');
}

if(isset($_POST['desc'])) {
    $description = mb_substr($pdo->quote(trim($_POST['desc'])), 1, -1);
}

//we checked isset previous so now add POST variables to local variables
$petId = mb_substr($pdo->quote(trim($_POST['pet_id'])), 1, -1);
$doctorId = mb_substr($pdo->quote(trim($_POST['doctor_id'])), 1, -1);
$serviceId = mb_substr($pdo->quote(trim($_POST['service'])), 1, -1);
$date = mb_substr($pdo->quote(trim($_POST['date'])), 1, -1);
$price = mb_substr($pdo->quote(trim($_POST['price'])), 1, -1);

//get current date (format:'2022-01-01')
$dateNow = date('Y-m-d');

//Validate data
if($date != $dateNow OR $doctorId != $_SESSION['id'] OR $petId != $_SESSION['petId']){
    redirection('index.php?r=14');
}

//Create object for medical card
if($operation = new medicalcards(0, $petId, $doctorId, $serviceId, $date, $price, $description)){

    //Add object data to database
    if(insertMedicalCard($operation->getPet(), $operation->getDoctor(), $operation->getService(), $operation->getDate(), $operation->getPrice(), $operation->getDescription())){
        redirection('index.php?op=animal&r=15');
    }else{
        redirection('index.php?op=karton&r=13');
    }

}else{
    redirection('index.php?op=karton&r=13');
}