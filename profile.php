<?php
//Profile page for users

require_once "functions_def.php";

global $pdo;

if (!isset($_SESSION['email']) OR !isset($_SESSION['id']) OR !is_int($_SESSION['id']) OR !isset($_SESSION['role'])) {
    redirection('login.php?l=0');
}

$id=$_SESSION['id'];
$sql = "SELECT firstname, lastname, image, email FROM users WHERE id=$id";
$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<div id="forms">
<form action="" method="POST" id="profile_form">
    <center><img src="images/loading_icon.gif" alt="Loading..." id="loading" class="hidden" height="30px"></center>
    <img src="<?php foreach ($results as $row) echo $row['image']; ?>" alt="Profile picture" id="pfp" class="img-fluid"><br>
    <label class="form-label" for="firstname">First name: </label><input type="text" class="form-control form-control-sm w-2" id="firstname" name="firstname" value="<?php
    foreach ($results as $row) echo $row['firstname'];?>"><br>
    <label class="form-label" for="lastname">Last name: </label><input type="text" class="form-control form-control-sm" id="lastname" name="lastname" value="<?php
    foreach ($results as $row) echo $row['lastname'];?>"><br>
    <label class="form-label" for="pic_upload">Profile picture: </label><input type="file" class="form-control form-control-sm" id="pic_upload" name="pic_upload" accept=".jpg,.png"><br>
    <label class="form-label" for="email">Email: </label><input type="email" id="email" class="form-control form-control-sm" name="email" value="<?php
    foreach ($results as $row) echo $row['email'];?>"><br>
    <label class="form-label" for="password">Password: </label><input type="password" class="form-control form-control-sm" id="password" name="password"><br>
    <label class="form-label" for="password2">Password again: </label><input type="password" class="form-control form-control-sm" id="password2" name="password2"><br>
    <input type="hidden" value="<?php foreach ($results as $row) echo $row['image'];?>" name="picture" id="picture" readonly>
    <input type="hidden" value="update" name="operation" id="update">
    <center><input class="btn btn-dark" type="submit" value="Update My Profile" onclick="validate(email, event)"></center>
</form>
    
<?php if ($_SESSION['role'] != 'doctor') { ?>
<form action="" method="POST" id="profile_form_pets">
    <center><img src="images/loading_icon.gif" alt="Loading..." id="loading2" class="hidden" height="30px"></center>
    <label class="form-label" for="petname">Pet name: </label><input type="text" class="form-control form-control-sm w-2" id="petname" name="petname"><br>
    <label class="form-label" for="petbirth">Birthdate: </label><input type="text" class="form-control form-control-sm w-2" id="petbirth" name="petbirth"><br>
    <label class="form-label" for="petimage">Image: </label><input type="file" class="form-control form-control-sm w-2" id="petimage" name="petimage"><br>
    <label class="form-label" for="breed">Breed: </label><select class="form-select" aria-label="Animal breed" id="breed" name="breed">
        <option value="0" selected>Choose the breed of your animal</option>
        <?php
        $sql = "SELECT id, name FROM breed";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $row) {
            echo "<option value='{$row['id']}'>{$row['name']}</option>";
        }
        ?>
    </select><br>
    <input type="hidden" value="" name="petPicture" id="petPicture" readonly>
    <input type="hidden" value="insert" name="operation" id="insert">
    <center><input class="btn btn-dark" type="submit" value="Insert Pet" onclick="validatePet(event)"></center>
</form>
<?php } ?>

<?php if ($_SESSION['role'] == 'doctor') { ?>
<form action="" method="POST" id="schedules_form">
    <label class="form-label" for="workdate">Work date: </label><input type="date" class="form-control form-control-sm w-2" value="<?= date('Y-m-d') ?>" id="workdate" name="workdate" min="<?= date('Y-m-d') ?>"><br>
    <label class="form-label" for="starthour">Start hour: </label><input type="number" class="form-control form-control-sm w-2" id="starthour" name="starthour" min="0" max="24"><br>
    <label class="form-label" for="endhour">End hour: </label><input type="number" class="form-control form-control-sm w-2" id="endhour" name="endhour" min="0" max="24"><br>
    <label class="form-label" for="holiday">Holiday: </label><select class="form-select" aria-label="Is holiday?" id="holiday" name="holiday">
        <option value="0" selected>No</option>
        <option value="1">Yes</option>
    </select><br>
    <label class="form-label" for="weekend">Weekend: </label><select class="form-select" aria-label="Is weekend?" id="weekend" name="weekend">
        <option value="0" selected>No</option>
        <option value="1">Yes</option>
    </select><br>
    <input type="hidden" value="<?= $_SESSION['id'] ?>" name="id" id="id">
    <input type="hidden" value="insertSchedule" name="operation" id="insertSchedule">
    <center><input class="btn btn-dark" type="submit" value="Insert Schedule" onclick="validateSchedule(event)"></center>
</form>
<?php } ?>
</div>
<?php

if ($_SESSION['role'] == 'doctor') {
    $doc_id = $_SESSION['id'];

    $sql = "SELECT r.user_id AS 'user_id', r.doc_id, r.pets_id AS 'pet', r.date AS 'date', r.hour AS 'hour', r.is_urgent, r.service_id, u.firstname AS 'firstname', u.lastname AS 'lastname', p.name 'petname', s.name as 'servicename' FROM reservations r 
        INNER JOIN users u ON r.user_id = u.id 
        INNER JOIN pets p ON r.pets_id = p.id
        INNER JOIN services s ON r.service_id = s.id
        WHERE doc_id = $doc_id
        ORDER BY r.date ASC, r.hour";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    if (count($results) > 0) {
    ?>
<div id='schedules_list'>
    <table class='sch table table-hover'>
        <tr class='table-active'>
            <th>Date</th>
            <th>Hour</th>
            <th>Owner</th>
            <th>Pet</th>
            <th>Service</th>
            <th>Penalty</th>
            <th></th>
        </tr>
        <?php

        foreach ($results as $row) {

            echo "
            <tr>
                <td>{$row['date']}</td>
                <td>{$row['hour']}</td>
                <td>{$row['firstname']} {$row['lastname']}</td>
                <td>{$row['petname']}</td>
                <td>{$row['servicename']}</td>
                <td>
                    <form action='index.php' method='GET'>
                        <input id='user_id' name='user_id' type='hidden' value='{$row['user_id']}' readonly>
                        <input id='oper' name='operation' type='hidden' value='penalty' readonly>
                        <button class='btn btn-dark' type='submit' >Add</button>
                    </form>
                </td>
                <td>
                    <form action='index.php?op=animal' method='POST'>
                        <input id='pet_id' name='pet_id' type='hidden' value='{$row['pet']}' readonly>
                        <button class='btn btn-dark' type='submit' >Operate</button>
                    </form>
                </td>
            </tr>
            ";
        }
        ?>
    </table>
</div>

<?php } } else {
    $user_id = $_SESSION['id'];

    $sql = "SELECT r.id AS 'reservation_id', r.user_id, r.doc_id AS 'doc_id', r.pets_id AS 'pet', r.date AS 'date', r.hour AS 'hour', r.is_urgent, r.service_id, u.firstname AS 'firstname', u.lastname AS 'lastname', p.name 'petname', s.name as 'servicename' FROM reservations r 
        INNER JOIN users u ON r.doc_id = u.id 
        INNER JOIN pets p ON r.pets_id = p.id
        INNER JOIN services s ON r.service_id = s.id
        WHERE r.user_id = $user_id
        ORDER BY r.date ASC, r.hour ";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        if (count($results) > 0) {
    ?>
<div id='schedules_list'>
    <table class='sch table table-hover'>
        <tr class='table-active'>
            <th>Date</th>
            <th>Hour</th>
            <th>Doctor</th>
            <th>Pet</th>
            <th>Service</th>
            <th><img src='images/loading_icon.gif' id='loading_icon1' class='hidden' style='width:2rem;'></th>
            <th></th>
        </tr>
        <?php
        foreach ($results as $row) {
            echo "
            <tr>
                <td>{$row['date']}</td>
                <td>{$row['hour']}</td>
                <td>{$row['firstname']} {$row['lastname']}</td>
                <td>{$row['petname']}</td>
                <td>{$row['servicename']}</td>
                <td>
                    <a class='btn btn-warning update' id='upd' href='admin/rows.php?id={$row['reservation_id']}&select=reservations'>Change</a>
                </td>
                <td>
                    <form action='index.php' method='GET'>
                        <input id='reservation_id' name='reservation_id' type='hidden' value='{$row['reservation_id']}' readonly>
                        <input id='date' name='date' type='hidden' value='{$row['date']}' readonly>
                        <input id='hour' name='hour' type='hidden' value='{$row['hour']}' readonly>
                        <input id='operation' name='operation' type='hidden' value='reservation_del' readonly>
                        <button class='btn btn-danger' type='submit' >Delete</button>
                    </form>
                </td>
            </tr>
            ";
        }
        ?>
    </table>
    <form action="" method="GET" id="update_schedules" class="hidden w-25" style='margin:auto;'>
    <label class="form-label" for="date1">Date: </label><input type="date" class="form-control form-control-sm" value="<?= date('Y-m-d') ?>" id="date1" name="date1" min="<?= date('Y-m-d') ?>"><br>
    <label class="form-label" for="hour1">Hour: </label><input type="number" class="form-control form-control-sm" id="hour1" name="hour1" min="0" max="24"><br>

    <input type="hidden" value="" name="id" id="id">
    <input type="hidden" value="reservation_upd" name="operation" id="reservation_upd">
    <center><input class="btn btn-dark" type="submit" value="Change Reservation" onclick="validateReservation(event)"></center>
</form>
</div>
<?php } } ?>

<script src="script.js"></script>
<script>
    var loading = document.getElementById("loading");
    var loading2 = document.getElementById("loading2");
    var pic_upload = document.getElementById("pic_upload");
    var pfp = document.getElementById("pfp");


    //Upload profile picture
    pic_upload.onchange = function() {
        pic_upload.disabled = true;
	    let file = this.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);

    reader.onload = function () {
        loading.classList.remove("hidden");
        let form_data = new FormData();
        form_data.set('image', reader.result.replace(/^.*?base64,/, ''));
        form_data.set('key', '733317fbddeda8ff5bd4ec1d2ec0a28d');
        fetch('https://api.imgbb.com/1/upload', {
            method: 'POST',
            body: form_data
        })
        .then(response => response.json())
        .then(json => {
            loading.classList.add("hidden");
            pfp.src = json.data.url;
            pfp.classList.remove("hidden");
            console.log(json);
            // nem csak url-t ad vissza hanem tobb adatot is, konzolban meg lehet oket nezni
            picture.value = json.data.url;
        })
    };
}

    //Upload animal image
    var petImage = document.getElementById('petimage');
    var petPicture = document.getElementById('petPicture');

    petImage.onchange = function() {
    petImage.disabled = true;
	let ffile = this.files[0];
    var freader = new FileReader();
    freader.readAsDataURL(ffile);

        freader.onload = function () {
            loading2.classList.remove("hidden");
            let form_data = new FormData();
            form_data.set('image', freader.result.replace(/^.*?base64,/, ''));
            form_data.set('key', '733317fbddeda8ff5bd4ec1d2ec0a28d');
            fetch('https://api.imgbb.com/1/upload', {
                method: 'POST',
                body: form_data
            })
            .then(response => response.json())
            .then(json => {
                loading2.classList.add("hidden");
                console.log(json);
                petPicture.value = json.data.url;
                console.log(petPicture.value);
            })
        };
    }

    //update profile data
    var loading_icon = document.getElementById('loading_icon1');

    for (let item of document.querySelectorAll('a.update')) {
        item.onclick = function(e) {
        update_schedules.classList.add('hidden');
        loading_icon.classList.remove('hidden');
        e.preventDefault();
        let url = this.href;
        let data;
        fetch(url)
            .then(response => response.json())
            .then(json => {
            update_schedules.classList.remove('hidden');
            loading_icon.classList.add('hidden');
            date1.value = json.date;
            hour1.value = json.hour;
            id.value = json.id;
            window.location = 'index.php?op=profile#update_schedules';
        });
    }
}
</script>