<?php

class medicalcards
{
    public $id;
    public $pet;
    public $doctor;
    public $service;
    public $date;
    public $price;
    public $description;

    public function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    public function getPet()
    {
        return $this->pet;
    }

    public function setPet($pet)
    {
        $this->pet = $pet;
    }

    public function getDoctor()
    {
        return $this->doctor;
    }

    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function __construct(int $id, string $pet, string $doctor, string $service, string $date, int $price, string $description)
    {
        $this->id = $id;
        $this->pet = $pet;
        $this->doctor = $doctor;
        $this->service = $service;
        $this->date = $date;
        $this->price = $price;
        $this->description = $description;
    }

    public  function getAllCards($petId){

        return $petId;
    }

}