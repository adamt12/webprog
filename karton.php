<?php
//List of animals (pets), doctors and admins can see all of animal users only own pet(s)

require_once "functions_def.php";

//guest can't access to this page
if (!isset($_SESSION['email']) OR !isset($_SESSION['id']) OR !is_int($_SESSION['id']) OR !isset($_SESSION['role'])) {
    redirection('login.php?l=0');
}

global $pdo;
?>
<h1 style="text-align: left;">ANIMALS OF THE DATABASE</h1>
<div id="animals">

    <?php
    $role = $_SESSION['role']; $id = $_SESSION['id'];
    $id_pet = 0; $owner = ""; $name = ""; $birth = ""; $breed = ""; $image = "images/pamacs.png"; $sql = ""; //$animalSite = 'window.location="index.php?op=animal;"';

    //We check for 'user' or something else ('admin' / 'doctor') and select the animals
    if($role == 'user') {
        $sql = "SELECT pets.id, pets.name, pets.name AS 'petname', pets.birth, pets.image, users.firstname, users.lastname, breed.name AS 'breedname' FROM `pets` 
                    INNER JOIN `users` ON pets.user_id = users.id 
                    INNER JOIN `breed` ON pets.breed_id = breed.id 
                    WHERE pets.user_id = '$id' ";
        $btnText = "View";
    }else{
        $sql = "SELECT pets.id, pets.name, pets.name AS 'petname', pets.birth, pets.image, users.firstname, users.lastname, breed.name AS 'breedname' FROM `pets` 
                    INNER JOIN `users` ON pets.user_id = users.id 
                    INNER JOIN `breed` ON pets.breed_id = breed.id";
        $btnText = "Operate";
    }
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    //Fill the tables with selected animals
    foreach ($results as $row) {
        $owner = $row['firstname'] . " " . $row['lastname'];
        $name = $row['petname'] . "";
        $birth = $row['birth'] . "";
        $breed = $row['breedname'] . "";
        $id_pet = $row['id'];
        $image = $row['image'];

        echo "
        <table class='animal'>
            <tr>
                <td>Owner:</td><td>$owner</td>
            </tr>
            <tr>
                <td>Name:</td><td>$name</td>
            </tr>
            <tr>
                <td>Birth:</td><td>$birth</td>
            </tr>
            <tr>
                <td>Breed:</td><td>$breed</td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:center;'><img src='$image' alt='Picture of the animal.'></td>
            </tr>
            <tr>
                <td colspan='2' >
                    <form action='index.php?op=animal' method='POST'>
                        <input id='pet_id' name='pet_id' type='hidden' value='$id_pet' readonly>
                        <input id='name' name='name' type='hidden' value='$name' readonly>
                        <input id='birth' name='birth' type='hidden' value='$birth' readonly>
                        <input id='image' name='image' type='hidden' value='$image' readonly>
                        <button type='submit' >$btnText</button>
                    </form>
                </td>
            </tr>
        </table>";
    }
    ?>
</div>