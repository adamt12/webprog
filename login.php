<?php
//login/register/forgotPassword page, we are using javascript to change form

//Delete old session (log out the user if he/she is here)
session_start();
$_SESSION = array();

require_once "db_config.php";
require_once "functions_def.php";

global $messages;

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1" id="metaViewport" name="viewport">
    <meta charset="UTF-8">
    <meta name="author" content="Porpoises">
    <meta name="description" content="">
    <meta name="robots" content="index, follow">
    <title>Login</title>
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <style>
        div:first-of-type {
        background-color: #000000;
        background-image: linear-gradient(315deg, #000000 0%, #414141 74%);
        display: grid;
        height: 100vh;
        color: white;
        align-items: center;
        text-align: center;
    }
    table {
        margin: auto;
    }
    button {
        background-color: #c2fbd7;
        border-radius: 100px;
        box-shadow: rgba(44, 187, 99, .2) 0 -25px 18px -14px inset,rgba(44, 187, 99, .15) 0 1px 2px,rgba(44, 187, 99, .15) 0 2px 4px,rgba(44, 187, 99, .15) 0 4px 8px,rgba(44, 187, 99, .15) 0 8px 16px,rgba(44, 187, 99, .15) 0 16px 32px;
        color: green;
        cursor: pointer;
        display: inline-block;
        font-family: CerebriSans-Regular,-apple-system,system-ui,Roboto,sans-serif;
        padding: 7px 20px;
        text-align: center;
        text-decoration: none;
        transition: all 250ms;
        border: 0;
        font-size: 16px;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
        margin: 1rem 0.5rem;
    }
    td {
        text-align: right;
    }
    a{
        color: white;
    }
    button:hover {
        background-color: white;
        color: black;
    }
    input:focus {
        outline: none !important;
        border-color: #719ECE;
        box-shadow: 0 0 10px #719ECE;
    }
    tr:last-child {
        border-spacing: 0 1rem;
    }
    td a {
        font-size: 80%;
    }
    button:hover {
        box-shadow: rgba(44,187,99,.35) 0 -25px 18px -14px inset,rgba(44,187,99,.25) 0 1px 2px,rgba(44,187,99,.25) 0 2px 4px,rgba(44,187,99,.25) 0 4px 8px,rgba(44,187,99,.25) 0 8px 16px,rgba(44,187,99,.25) 0 16px 32px;
        transform: scale(1.05) rotate(-1deg);
    }
    a:hover {
        color: #90EE90;
    }
    .invisible {
        display: none;
    }

    </style>
</head>
<body>
    <div>
        <form action="web.php" method="POST">
            <span class="invisible" id="forgotText">Tell us your email address, and we'll send you a link to reset your password.</span>
            <table id="login">
                <tr>
                    <td><label for="email">Email</label></td><td><input type="email" name="email" id="email"></td>
                </tr>
                <tr id="trPass">
                    <td><label for="password">Password</label></td><td><input type="password" name="password" id="password" minlength="8"></td>
                </tr>
                <tr>
                    <td><a id="forgot" title="Forgot your password?" href="#" onclick="forgot();return false;">Forgot?</a></td><td><a href="#" onclick="register();return false;">Register</a></td>
                </tr>
            </table>

            <table id="register" class="invisible">
                <tr>
                    <td><label for="fname">First Name</label></td><td><label for="lname">Last Name</label></td>
                </tr>
                <tr>
                    <td><input name="fname" id="fname" type="text"></td><td><input name="lname" id="lname" type="text"></td>
                </tr>
                <tr>
                    <td><label for="phone">Phone Number</label></td><td><label for="emailReg">Email</label></td>
                </tr>
                <tr>
                    <td><input name="phone" id="phone" type="text"></td><td><input name="emailReg" id="emailReg" type="email"></td>
                </tr>
                <tr>
                    <td><label for="passwordReg">Password</label></td><td><label for="passwordConfirm">Confirm Password</label></td>
                </tr>
                <tr>
                    <td><input name="passwordReg" id="passwordReg" type="password" minlength="8"></td><td><input name="passwordConfirm" id="passwordConfirm" type="password" minlength="8"></td>
                </tr>
            </table>
            <input type="hidden" id="action" name="action" value="login">
            <button type="button" id="backBt" onclick="goBack()">Back</button>
            <button id="loginBt" type="submit" onclick="validateLogin(event)">Login</button>
        </form>
    </div>
<?php
//Alert if GET variable [r] isset
$r = 0;

if (isset($_GET["r"]) and is_numeric($_GET['r'])) {
    $r = (int)$_GET["r"];

    if (array_key_exists($r, $messages)) {

        echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('$messages[$r]');
                }, 100); 
              </script>";
    }
}

//Alert if GET variable [l] isset
$l = 0;

if (isset($_GET["l"]) and is_numeric($_GET['l'])) {
    $l = (int)$_GET["l"];

    if (array_key_exists($l, $messages)) {

        echo "<script type='text/javascript'>
                window.setTimeout(function(){
                    alert('$messages[$l]');
                }, 100); 
              </script>";
    }
}
?>

<?php //JS validation ?>
<script>
    
    var pos = ""; 
    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    function validateEmail(email) {
    if (email.value.match(validRegex)) 
        return true;
    else
        return false;
    }
    function forgot() {
        document.getElementById("trPass").classList.toggle("invisible");
        document.getElementById("forgot").classList.toggle("invisible");
        document.getElementById("forgotText").classList.toggle("invisible");
        document.getElementById("loginBt").innerHTML = "Reset Password";
        document.getElementById("action").value = "forget";
        pos = 'forgot';
    }
    function register() {
        document.getElementById("login").classList.toggle("invisible");
        document.getElementById("register").classList.toggle("invisible");
        document.getElementById("loginBt").innerHTML = "Register";
        if(!document.getElementById("forgotText").classList.contains("invisible"))
        document.getElementById("forgotText").classList.toggle("invisible");
        document.getElementById("action").value = "register";
        pos = 'register';
    }
    function goBack() {
        if(pos === 'forgot' || pos === 'register') {
            window.location.href = 'login.php';
            pos = "";
        }
        else {
            window.location.href = 'index.php';
            pos = "";
        }
    }
    function validateLogin(event) {
        if (pos == 'register') {
            let password1 = document.getElementById('passwordReg');
            let password2 = document.getElementById('passwordConfirm');
            let emailReg = document.getElementById('emailReg');
            let fname = document.getElementById('fname');
            let lname = document.getElementById('lname');
            let phone = document.getElementById('phone');

            if (validateEmail(emailReg)) {
                if (password1.value == password2.value) {
                    if (password1.value.length < 8) {
                        alert("The password has to be at least 8 characters long!");
                        event.preventDefault();
                    }
                    else {
                        if (fname.value == '' || lname.value == '' || phone.value == '') {
                            alert('Name or phone can not be empty!');
                            event.preventDefault();
                        }
                        else
                            return true;
                    }
                }
                else {
                    alert("Passwords does not match!");
                    event.preventDefault();
                }
            }
            else {
                alert("Invalid email address!");
                event.preventDefault();
            }
        }
        else if (pos == 'forgot') {
            if (validateEmail(email)) {
                alert('You will soon receive an email to reset your password.');
                return true;
            }
            else {
                alert('Invalid email address!');
                event.preventDefault();
            }
        }
        else {
            let password = document.getElementById('password');
            if (validateEmail(email)) {
                if (password.value.length < 8) {
                    alert("The password has to be at least 8 characters long!")
                    event.preventDefault();
                }
                else
                    return true;
            }
            else {
                alert("Invalid email address!");
                event.preventDefault();
            }
        }
    }


</script>
</body>
</html>