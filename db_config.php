<?php
/* Use this for server */
/*define("SITE", "https://porpoises.proj.vts.su.ac.rs/"); // enter your path on localhost
define("HOST", "localhost");
define("USER", "porpoises");
define("PASSWORD", "5gvs6449qx8j6lW");
define("DATABASE", "porpoises");
define("SECRET", "zs+g!5E4vq#abQx0f");*/

/* Use this for localhost */
define("SITE", "https://localhost:8013/AllatorvosiRendelo"); // enter your path on localhost
define("HOST", "localhost");
define("USER", "root");
define("PASSWORD", "password");
define("DATABASE", "porpoises");
define("SECRET", "zs+g!5E4vq#abQx0f");

$actions = ['login', 'register', 'forget'];

//messages text
$messages = [
    0 => 'No direct access!',
    1 => 'Unknown user!',
    2 => 'User with this email already exists, choose another one!',
    3 => 'Check your email to active your account!',
    4 => 'Fill all the fields!',
    5 => 'You are logged out!!',
    6 => 'Your account is activated, you can login now!',
    7 => 'Passwords are not equal!',
    8 => 'Format of e-mail address is not valid!',
    9 => 'Password is too short! It must be minimum 8 characters long!',
    10 => 'Something went wrong with mail server. We will try to send email later!',
    11 => 'Your account is already activated!',
    12 => 'Your password reset successfully!',
    13 => 'Something went wrong!',
    14 => 'Invalid values!',
    15 => 'Successful data insertion!'
];

//pdo variable declare
if (defined('SECRET') and SECRET == 'zs+g!5E4vq#abQx0f') {

    try {
        $pdo = new PDO("mysql:host=" . HOST . ";dbname=" . DATABASE,
            USER, PASSWORD,
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]);
    } catch (PDOException $e) {
        exit("Error: " . $e->getMessage());
    }
}
