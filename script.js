function $(id) {
    return document.getElementById(id);
}

var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

function validate(email, event) {
    let password = $('password');
    let password2 = $('password2');
    if (email.value.match(validRegex)) {
        if (password.value == password2.value) {
            if (password.value.length < 8) {
                alert("The password has to be at least 8 characters long!");
                event.preventDefault();
            }
            else {
                return true;
            }
        }
        else {
            alert("Passwords does not match!");
            event.preventDefault();
        }
    }
    else {
      alert("Invalid email address!");
      event.preventDefault();
    }
}

function validatePet(event) {
    let breed = $('breed');
    if (breed.value != 0) {
        return true;
    }
    else {
        alert("Please select the breed of your animal!");
        event.preventDefault();
    }
}

function validateSchedule(event) {
    if ($('starthour').value > 24 || $('starthour').value < 1) {
        alert('Start hour must be between 0 and 24');
        event.preventDefault();
        return false;
    }
    else if ($('endhour').value > 24 || $('endhour').value < 1) {
        alert('End hour must be between 0 and 24');
        event.preventDefault();
        return false;
    }
    else 
        return true;
}

function validateAppointment(event) {
    if ($('urgent').checked) {
        $('urgent_hidden').disabled = true;
    }
    if ($('pets_id').value == '') {
        alert('Choose a pet.');
        event.preventDefault();
        return false;
    }
    else if ($('hour').value == '') {
        alert('Hour can not be empty.');
        event.preventDefault();
        return false;
    }

    return true;
}

function validateReservation(event) {
    if ($('date1').value == '') {
        alert('Select a date!');
        event.preventDefault();
        return false;
    }
    else if ($('hour1').value == '') {
        alert('Hour can not be empty.');
        event.preventDefault();
        return false;
    }

    return true;
}

function validateAnimalUpdate(event) {
    if ($('name').value == '') {
        alert('Name can not be empty!');
        event.preventDefault();
        return false;
    }
    else if ($('birth').value == '') {
        alert('Birth can not be empty.');
        event.preventDefault();
        return false;
    }
    else if ($('picture').value == '') {
        alert('Picture can not be empty.');
        event.preventDefault();
        return false;
    }
    else
        return true;
}

function isUrgent(firstDate, firstHour) {
    var checkBox = document.getElementById('urgent');
    var date = document.getElementById('date');
    var hour = document.getElementById('hour');
    if(checkBox.checked === true){
        date.readOnly = true;
        hour.readOnly = true;
        date.value = firstDate;
        hour.value = firstHour;
    }else {
        date.readOnly = false;
        hour.readOnly = false;
        date.value = new Date().toISOString().slice(0, 10);
        hour.value = null;
    }
}