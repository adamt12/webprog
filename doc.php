<?php
//Doctor profile page

require_once "functions_def.php";
require_once "db_config.php";

global $pdo;
global $loginState;

$id = $_GET['id'] ?? '';
$name = ""; $department = ""; $email = ""; $phone = ""; $image = "";
$sql = "SELECT id, image, firstname, lastname, department, email, phone FROM users WHERE status='doctor' AND id=$id";
$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<div id="schedules">

<?php

foreach ($results as $row) {
    $name = $row['firstname'] . " " . $row['lastname'];
    $department = $row['department'] . "";
    $email = $row['email'] . "";
    $phone = $row['phone'] . "";
    $image = $row['image'];
    $id = $row['id'];
}
echo "<div id='doc'>
          <img src='$image' alt='Image of the doctor' class='docImg'>
          <div class='docTexts'>
              <p class='name'>$name</p>
              <p class='department'>$department</p>
              <p class='email'>$email</p>
              <p class='phone'>$phone</p>
          </div>";

//variables to save first valid date and hour it will use for urgent situations
$firstDate = ""; $firstHour = "";

if ($loginState) {
    $doc_id = $_GET['id'];
    $sql = "SELECT * FROM schedules WHERE doc_id=$doc_id AND work_date > CURRENT_DATE";
    $query = $pdo->prepare($sql);
    $query->execute();

    if($results = $query->fetchAll(PDO::FETCH_ASSOC)) {
        echo "<div id='sch_container'><table class='sch table table-hover w-25'>
                  <tr class='table-active'>
                      <th>Date</th>
                      <th>Time</th>
                  </tr>";
        //counter
        $i = 0;

        foreach ($results as $row) {

            if ($row['is_holiday'] == 0 && $row['is_weekend'] == 0 && $row['is_taken'] == 0) {

                echo "<tr><td>{$row['work_date']}</td><td>{$row['start_workhour']}:00</td></tr>";

                if($i===0) {
                    $firstDate = $row['work_date'];
                    $firstHour = $row['start_workhour'];
                }
                $i++;
            }
        }
        $fail = false;

        echo "</tr></table></div>";
    } else {

        echo "<p>This doctor is not available at the moment.</p>";
        $fail = true;
    }
}
?>

<div id="appointments">
    <?php if ($loginState) { ?>
    <h1>Appointments</h1>
    <form action="" method="GET">
        <table>
            <tr>
                <td><label for="pets_id">Pet </label></td>
                <td>
                    <select class="form-select" name="pets_id" id="pets_id">

                    <?php
                    $id = $_SESSION['id'];
                    $sql = "SELECT * FROM pets WHERE user_id = $id";
                    $query = $pdo->prepare($sql);
                    $query->execute();
                    $results = $query->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($results as $row) {

                        echo "<option value='{$row['id']}'>{$row['name']}</option>";
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="date">Date </label></td>
                <td><input class="form-control form-control-sm w-2" type="date" name="date" id="date" value="<?= date('Y-m-d') ?>" min="<?= date('Y-m-d') ?>"></td>
            </tr>
            <tr>
                <td><label for="date">Hour </label></td>
                <td><input class="form-control form-control-sm w-2" type="number" name="hour" id="hour" min="0" max="24"></td>
            </tr>
            <tr>
                <td><label class="form-check-label" for="urgent">Urgent </label></td>
                <input type='hidden' value='0' name='urgent' id="urgent_hidden">
                <td><input class="form-check-input" type="checkbox" name="urgent" id="urgent" value="1" onchange="isUrgent(<?php echo "'$firstDate', '$firstHour'"; ?>)"></td>
            </tr>
            <tr>
                <td><label for="service">Service </label></td>
                <td>
                    <select class="form-select" name="service" id="service">

                    <?php
                    $sql = "SELECT services.id, services.name FROM services INNER JOIN doctor_service ON doctor_service.service_id = services.id WHERE doctor_service.user_id = ?";
                    $query = $pdo->prepare($sql);
                    $query->bindParam(1, $_GET['id'], PDO::PARAM_INT);
                    $query->execute();
                    $results_services = $query->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($results_services as $rowServices) {
                        $serviceId = $rowServices['id'];
                        $serviceName = $rowServices['name'];

                        echo "<option value='$serviceId'>$serviceName</option>";
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <input type="hidden" name="operation" value="insertAppointment">
                <input type="hidden" name="doc_id" value="<?= $_GET['id']; ?>">
                <td colspan="2"><input <?php if ($fail) echo "disabled"?> class="btn btn-dark" type="submit" value="Appoint" onclick="validateAppointment(event)"></td>
            </tr>
        </table>
    </form>
    <?php
    //if guest watch the site
    } else { ?>
    <h1>Services</h1>
        <select class="form-select" name="service" id="service" style="width: 13rem; margin: auto" readonly>

        <?php
        $sql = "SELECT services.id, services.name FROM services INNER JOIN doctor_service ON doctor_service.service_id = services.id WHERE doctor_service.user_id = ?";
        $query = $pdo->prepare($sql);
        $query->bindParam(1, $_GET['id'], PDO::PARAM_INT);
        $query->execute();
        $results_services = $query->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results_services as $rowServices) {
            $serviceId = $rowServices['id'];
            $serviceName = $rowServices['name'];

            echo "<option value='$serviceId'>$serviceName</option>";
        }
    } 
    ?>

        </div>
    </div>
</div>

<script src="script.js"></script>
