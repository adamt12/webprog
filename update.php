<?php
//Update password page (use token from GET for validation)

require "db_config.php";
require "functions_def.php";

global $pdo;

$code = "";

if (isset($_GET['token'])){
    $token = mb_substr($pdo->quote(trim($_GET['token'])), 1, -1);
}

if (isset($_POST['password'])){
    $password = mb_substr($pdo->quote(trim($_POST['password'])), 1, -1);
    $passwordHashed = password_hash($password, PASSWORD_DEFAULT);
}

//Form refer here, and here is the check for the new password
if (!empty($token) AND strlen($token) === 40) {

    if(isset($passwordHashed) AND isset($password) AND strlen($password) > 8) {
        $sql = "UPDATE users SET password = ?, token='', registration_expires=''
            WHERE  binary token = ? AND registration_expires > now()";

        if ($pdo->prepare($sql)->execute([$passwordHashed, $token])) {
            redirection('index.php?r=12');
        }

    }

} else {
    redirection('index.php?r=0');
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1" id="metaViewport" name="viewport">
    <meta charset="UTF-8">
    <meta name="author" content="Porpoises">
    <meta name="description" content="">
    <meta name="robots" content="index, follow">
    <title>Login</title>
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <style>
    div:first-of-type {
        background-color: #000000;
        background-image: linear-gradient(315deg, #000000 0%, #414141 74%);
        display: grid;
        height: 100vh;
        color: white;
        align-items: center;
        text-align: center;
    }
    table {
        margin: auto;
    }
    button {
        background-color: #c2fbd7;
        border-radius: 100px;
        box-shadow: rgba(44, 187, 99, .2) 0 -25px 18px -14px inset,rgba(44, 187, 99, .15) 0 1px 2px,rgba(44, 187, 99, .15) 0 2px 4px,rgba(44, 187, 99, .15) 0 4px 8px,rgba(44, 187, 99, .15) 0 8px 16px,rgba(44, 187, 99, .15) 0 16px 32px;
        color: green;
        cursor: pointer;
        display: inline-block;
        font-family: CerebriSans-Regular,-apple-system,system-ui,Roboto,sans-serif;
        padding: 7px 20px;
        text-align: center;
        text-decoration: none;
        transition: all 250ms;
        border: 0;
        font-size: 16px;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
        margin: 1rem 0.5rem;
    }
    td {
        text-align: right;
    }
    a{
        color: white;
    }
    button:hover {
        background-color: white;
        color: black;
    }
    input:focus {
        outline: none !important;
        border-color: #719ECE;
        box-shadow: 0 0 10px #719ECE;
    }
    tr:last-child {
        border-spacing: 0 1rem;
    }
    td a {
        font-size: 80%;
    }
    button:hover {
        box-shadow: rgba(44,187,99,.35) 0 -25px 18px -14px inset,rgba(44,187,99,.25) 0 1px 2px,rgba(44,187,99,.25) 0 2px 4px,rgba(44,187,99,.25) 0 4px 8px,rgba(44,187,99,.25) 0 8px 16px,rgba(44,187,99,.25) 0 16px 32px;
        transform: scale(1.05) rotate(-1deg);
    }
    a:hover {
        color: #90EE90;
    }
    .invisible {
        display: none;
    }

    </style>
</head>
<body>
    <div>
        <form action="" method="POST">
            <table id="login">
                <tr>
                    <td><label for="password">Password</label></td>
                    <td><label for="passwordConfirm">Confirm Password</label></td>
                </tr>
                <tr>
                    <td><input name="password" id="password1" type="password" minlength="8"></td>
                    <td><input name="passwordConfirm" id="password2" type="password" minlength="8"></td>
                </tr>
            </table>
            <button id="loginBt" type="submit" onclick="validateLogin(event)">Set Password</button>
        </form>
    </div>
<script>
    function validateLogin(event) {
        let password1 = document.getElementById('password1');
        let password2 = document.getElementById('password2');

        if (password1.value == password2.value) {
            if (password1.value.length < 8) {
                alert("The password has to be at least 8 characters long!");
                event.preventDefault();
            }
            else
                return true;
        }
        else {
            alert("Passwords does not match!");
            event.preventDefault();
        }
    }
</script>
</body>
</html>