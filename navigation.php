<?php

global $loginState;

$op="";

if(isset($_GET['op'])) {
    $op = $_GET['op'];
}
?>
<nav>
    <button id="menu_trigger">≡</button>
    <aside id="menu">
        <a id="logo" href="index.php?op=main">
            <img id="logoImg" src="images/logo.png" alt="Home">
        </a>
        <a href="index.php?op=doctors">Doctors</a>

        <?php
        $role = "";

        if(isset($_SESSION['role'])) {
            $role = $_SESSION['role'];
        }

        if($loginState AND $role == 'user') {

            echo "<a href='index.php?op=karton'>My pets</a>";
        }elseif($loginState){

            echo "<a href='index.php?op=karton'>E-karton</a>";
        }
        ?>

        <a href="index.php?op=contact">Contact</a>

        <?php

        if($loginState) {

            echo "<a id='login' href='index.php?op=profile'>";
        } else {

            echo "<a id='login' href='login.php'>";
        }
        ?>

        <img id="loginImg" src="images/login.png" alt="Login"
            <?php
                //If someone logged in add green border around profile
                if ($loginState) {
                echo "style='border-radius: 99rem; border: 2px solid #00ab41'";
            }
            ?>
        >

        <?php

        //If user logged in can see Log out link and can see his/her username (left side of the email)
        if($loginState) {
                //Separate email text (get username)
                $nickname = explode("@", $_SESSION['email']);

                echo "<span>" . $nickname[0] . "</span></a>";
                echo "<a href='logout.php'>Log out</a>";
            }
            else{
                echo "</a>";
            }
        ?>

    </aside>

    <?php //Mobile menu responsive ?>
    <script>
        menu_trigger.onclick = function() {
            menu.classList.toggle('menu-open');
            if (menu_trigger.innerText == '≡')
                menu_trigger.innerText = 'X';
            else 
                menu_trigger.innerText = '≡';
        }
    </script>
</nav>