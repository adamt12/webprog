<?php

require_once "db_config.php";

/**
 * Function redirects user to given url
 *
 * @param string $url
 */
function redirection($url)
{
    header("Location:$url");
    exit();
}

/**
 * Function checks that login parameters exists in users table
 *
 * @param string $username
 * @param string $password
 * @return array
 */
function checkUserLogin($email, $enteredPassword, $user)
{
    global $pdo;

    $sql = "SELECT id, password, status FROM users WHERE email = '$email' AND active=1 LIMIT 0,1";
    $query = $pdo->prepare($sql);
    $query->execute();
    $data = [];
    $registeredPassword = "";
    if ($query->rowCount() > 0) {
        while ($record = $query->fetch()) {
            $status = $record['status'];
            $data['id'] = (int)$record['id'];
            $data['role'] = $status;
            $registeredPassword = $record['password'];
        }

        if (!password_verify($enteredPassword, $registeredPassword) || ($status != 'admin' && $user == 'admin')) {
            $data['id'] = "Wrong";
        }
    }
    return $data;
}

/**
 * Function checks that user exists in users table
 *
 * @param $username
 * @return bool
 */
function existsUser($email)
{
    global $pdo;

    $sql = "SELECT id FROM users WHERE email = ? AND (registration_expires>now() OR active ='1')";
    $query = $pdo->prepare($sql);
    $query->execute([$email]);

    if ($query->fetchColumn() > 0) {

        return true;
    }else {

        return false;
    }
}

/**
 * Function registers user and returns id of created user
 *
 * @param $username
 * @param $password
 * @param $firstname
 * @param $lastname
 * @param $email
 * @param $code
 * @return int
 */
function registerUser($password, $firstname, $lastname, $email, $phoneNumber, $token)
{
    global $pdo;
    $passwordHashed = password_hash($password, PASSWORD_DEFAULT);
    $sql = "INSERT INTO users (password,firstname,lastname,email,token,registration_expires,active, phone)
             VALUES (?,?,?,?,?,DATE_ADD(now(),INTERVAL 1 DAY),?,?)";
    $pdo->prepare($sql)->execute([$passwordHashed, $firstname, $lastname,$email,$token,0,$phoneNumber]);

    $stmt = $pdo->query("SELECT LAST_INSERT_ID()");
    $lastId = $stmt->fetchColumn();

    return $lastId;
}

/**
 * Function creates code with given length and returns it
 *
 * @param $length
 * @return string
 */
function createCode($length)
{
    $down = 97;
    $up = 122;
    $i = 0;
    $code = "";
    $div = mt_rand(3, 9); // 3

    while ($i < $length) {

        if ($i % $div == 0) {
            $character = strtoupper(chr(mt_rand($down, $up)));
        }else {
            $character = chr(mt_rand($down, $up)); // mt_rand(97,122) chr(98)
        }

        $code .= $character; // $code = $code.$character; //
        $i++;
    }

    return $code;
}


require_once  "includes/PHPMailer.php";
require_once  "includes/SMTP.php";
require_once  "includes/Exception.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

/**
 * Function tries to send email with activation code
 *
 * @param $email
 * @param $token
 * @param $type
 * @return bool
 * @throws Exception
 */
function sendData($email, $token, $type)
{
    if($type == 'register'){
        $activateSite = "active.php?token=$token";
        $msg = 'to activate your account click on the link: ';
    }else {
        $activateSite = "update.php?token=$token";
        $msg = 'to update your password click on the link: ';
    }

    $message = "Data:\n\n user: $email \n \n www.vts.su.ac.rs ";
    $message .= "\n\n" . $msg . SITE . $activateSite;

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->Host = 'smtp.mailtrap.io';
    $mail->SMTPAuth = "true";
    $mail->SMTPSecure = "tls";
    $mail->Port = 2525;
    $mail->Username = 'ff06bfe2d6b1e7';
    $mail->Password = 'd5e012a356d24d';
    $mail->Subject = "Registration at Porpoises";
    $mail->setFrom("porpoises@vts.su.ac.rs");
    $mail->isHTML(true);
    $mail->Body = $message;
    $mail->addAddress($email);
    if($mail->Send()){
        $mail->smtpClose();
        return true;
    }else {
        $mail->smtpClose();
        return false;
    }
}

/**
 * Function tries to give token and interval to set new password
 * @param $email
 * @param $token
 * @return bool
 */
function updatePassword($email, $token)
{
    global $pdo;

    $status = false;

    $sql = "UPDATE users
            SET token = ?, registration_expires = DATE_ADD(now(),INTERVAL 1 HOUR)
            WHERE email = ?";

    if($pdo->prepare($sql)->execute([$token, $email])){
        $status = true;
    }

    return $status;
}

/**
 * Function check that the given petId is belongs to given user
 *
 * @param $userId
 * @param $petId
 * @return bool
 */
function checkUserPet($userId, $petId)
{
    global $pdo;

    $status = false;
    //$petId = 2; $userId = 14;
    $sql = "SELECT name FROM pets WHERE (user_id = ? AND id = ? ) OR (SELECT id from users WHERE id = ? AND (status = 'doctor' OR status = 'admin'))";
    $command = $pdo->prepare($sql);
    $command->bindParam(1, $userId, PDO::PARAM_INT);
    $command->bindParam(2, $petId, PDO::PARAM_INT);
    $command->bindParam(3, $userId, PDO::PARAM_INT);
    $command->execute();
    $row = $command->fetch(PDO::FETCH_ASSOC);
    if($row){
        $status = true;
    }

    return $status;
}

/**
 * Function tries to insert new medical card (store medical history)
 *
 * @param $petId
 * @param $doctorId
 * @param $serviceId
 * @param $date
 * @param $price
 * @param $description
 * @return bool
 */
function insertMedicalCard($petId, $doctorId, $serviceId, $date, $price, $description)
{
    global $pdo;

    $status = false;

    $sql = "INSERT INTO medicalcards(pet_id, doc_id, service_id, date, price, description) VALUES(?,?,?,?,?,?)";

    if($pdo->prepare($sql)->execute([$petId, $doctorId, $serviceId, $date, $price, $description])){
        $status = true;
    }

    return $status;
}

function generateToken()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $code = '';

    for ($i = 0; $i < 10; $i++) {
        $code .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $code;
}

function sendDataReservation($email, $code, $date, $hour)
{
    $msg = 'Your reservation accepted. Your code is: ';

    $message = "Data:\n\n user: $email \n \n On: " . SITE;
    $message .= "\n\n" . $msg . $code . " Your date is: " . $date . " hour is: " . $hour . ":00";

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->Host = 'smtp.mailtrap.io';
    $mail->SMTPAuth = "true";
    $mail->SMTPSecure = "tls";
    $mail->Port = 2525;
    $mail->Username = 'ff06bfe2d6b1e7';
    $mail->Password = 'd5e012a356d24d';
    $mail->Subject = "Reservation at Porpoises";
    $mail->setFrom("porpoises@vts.su.ac.rs");
    $mail->isHTML(true);
    $mail->Body = $message;
    $mail->addAddress($email);
    if($mail->Send()){
        $mail->smtpClose();
        return true;
    }else {
        $mail->smtpClose();
        return false;
    }
}