<?php

global $loginState;
global $pdo;

//set variables if every Session variable is set
if($loginState AND isset($_SESSION['role']) AND isset($_SESSION['petId']) AND isset($_SESSION['id'])){
    $role = $_SESSION['role'];
    $petId = $_SESSION['petId'];
    $docId = $_SESSION['id'];
} else{
    redirection('index.php?l=0');
}

$role = $_SESSION['role'];

if (isset($_GET['for'])) {
    $for = $_GET['for'];

    if ($for == 'new' AND ($role == 'doctor' OR $role == 'admin')) {
        $petName = ""; $doctorName = ""; $date = date('Y-m-d');
        $serviceId = ""; $serviceName = ""; $price = ""; $description = "";

        //Select pet name
        $sql = "SELECT name FROM `pets` WHERE id = ?";
        $query = $pdo->prepare($sql);
        $query->bindParam(1, $petId, PDO::PARAM_INT);
        $query->execute();
        $results_pet = $query->fetch(PDO::FETCH_ASSOC);
        $petName = $results_pet['name'];

        //Select doctor name
        $sql = "SELECT firstname, lastname FROM `users` WHERE id = ?";
        $query = $pdo->prepare($sql);
        $query->bindParam(1, $docId, PDO::PARAM_INT);
        $query->execute();
        $results_doctor = $query->fetch(PDO::FETCH_ASSOC);
        $doctorName = $results_doctor['firstname'] . " " . $results_doctor['lastname'];

        //Select available services
        $sql = "SELECT services.id, services.name FROM services INNER JOIN doctor_service ON doctor_service.service_id = services.id WHERE doctor_service.user_id = ?";
        $query = $pdo->prepare($sql);
        $query->bindParam(1, $docId, PDO::PARAM_INT);
        $query->execute();
        $results_services = $query->fetchAll(PDO::FETCH_ASSOC);


        echo "<form action='operation_insert.php' id='form1' name='form1' method='post'>
                <div class='labels'>
                    <label class='form-label' for='animal'>Animal</label>
                    <label class='form-label' for='doctor'>Doctor</label>
                    <label class='form-label' for='date'>Date</label>
                    <label class='form-label' for='service'>Service</label>
                    <label class='form-label' for='price'>Price</label>
                    <label class='form-label' for='desc'>Description</label>
                </div>
                <div class='inputs'>
                    <input class='form-control form-control-sm' type='text' id='animal' name='animal' value='$petName' aria-label='Name of the animal' readonly>
                    <input class='form-control form-control-sm' type='text' id='doctor' name='doctor' value='$doctorName' aria-label='Name of the doctor' readonly>
                    <input class='form-control form-control-sm' type='text' id='date' name='date' value='$date' aria-label='Current date' readonly >";

        echo "<select id='service' name='service' class='form-select form-select-sm' style='margin-top: .5rem;'>";

        //Set services
        foreach ($results_services as $rowServices) {
            $serviceId = $rowServices['id'];
            $serviceName = $rowServices['name'];

            echo "<option value='$serviceId'>$serviceName</option>";
        }

        echo "</select>
              <input class='form-control form-control-sm'  type='number' id='price' name='price' min='0' style='margin-top: .5rem;'>
              <textarea class='form-control form-control-sm' aria-label='Service' name='desc' id='desc' cols='30' rows='10'></textarea>
          </div>
          <input type='hidden' value='1' name='enable' id='enable'>
          <input type='hidden' value='$docId' name='doctor_id' id='doctor_id'>
          <input type='hidden' value='$petId' name='pet_id' id='pet_id'>
          <input class='btn btn-dark' type='submit' value='Insert Operation'>
      </form>";
    }
}
?>