<?php
//Activate user who want to register on page

require "db_config.php";
require "functions_def.php";

global $pdo;

$code = "";

if (isset($_GET['token'])){
    $token = mb_substr($pdo->quote(trim($_GET['token'])), 1, -1);
}

//Activate user if token is correct, registration not expired yet and there is less than 3 penalty point
if (!empty($token) AND strlen($token) === 40) {
    $sql = "UPDATE users SET active='1', token='', registration_expires=''
            WHERE  binary token = ? AND registration_expires>now() AND penalty < 3";

    if($pdo->prepare($sql)->execute([$token])){
        redirection('index.php?r=6');
    }else{
        redirection('index.php?r=11');
    }

} else {
    redirection('index.php?r=0');
}