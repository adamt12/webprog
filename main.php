<?php //Moving animated text on main page ?>
<div id='bar'>
    <span id="bar-content">
    <?php
    global $loginState;
    if (!$loginState)
        echo "Login to see the details of the page. You can make appointments with the doctor you select. If you want your pets healed, you've came to the right place.";
    else {
        global $pdo;
        $id = mt_rand(5,12);
        $sql = "SELECT * FROM users WHERE id = $id";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        echo "Need a <span class='highlight'>" . $results[0]['department'] . "</span>? Call <span class='highlight'>". $results[0]['phone'] . "</span>, or send an email to <span class='highlight'>" . $results[0]['email'] . "</span> to get in touch with our doctor. For further information, click on the Doctors tab.";
    }
    ?>
    </span>
</div>
