<?php
//Animal datasheet page

require_once "functions_def.php";
require_once "medicalcards.php";

global $pdo;

//Login validation because guests not allowed to see this page
if (!isset($_SESSION['email']) or !isset($_SESSION['id']) or !is_int($_SESSION['id']) or !isset($_SESSION['role'])) {
    redirection('login.php?l=0');
}

$userId = $_SESSION['id'];
$role = $_SESSION['role'];

//set petId from POST(if request coming from animal list) or from SESSION(if one animal page was opened yet)
if(isset($_POST['pet_id'])) {
    $petId = $_POST['pet_id'];
}elseif(isset($_SESSION['petId'])){
    $petId = $_SESSION['petId'];
}

if (isset($petId) AND !checkUserPet($userId, $petId)) {
    redirection('index.php?op=karton&r=0');
}

//If it is valid petId than we add this for session id
$_SESSION['petId'] = $petId;

//Owner and breed_id
$sql = "SELECT pets.breed_id, users.firstname, users.lastname FROM `pets` INNER JOIN `users` ON users.id = pets.user_id WHERE pets.id = $petId";
$query = $pdo->prepare($sql);
$query->execute();
$results_pet = $query->fetch(PDO::FETCH_ASSOC);

$breedId = $results_pet['breed_id'];
$user_name = $results_pet['firstname'] . " " . $results_pet['lastname'];

//Some other data of the animal
$image = ""; $name = ""; $birth = ""; $breed = "";
$sql = "SELECT pets.id, pets.name, pets.birth, pets.image, users.firstname AS 'user_firstname', users.lastname AS 'user_lastname', breed.name AS 'breed_name' 
        FROM `pets` 
        INNER JOIN `users` ON $userId = users.id 
        INNER JOIN `breed` ON $breedId = breed.id 
        WHERE pets.id = $petId";
$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetch(PDO::FETCH_ASSOC);

$name = $results['name']; //Pet_Name
$birth = $results['birth']; //Pet_birth
$breed = $results['breed_name']; //Peth_breed
$image = $results['image']; //pet image

echo "<h1>$name</h1>
    <div class='animal-karton'>
    <img src='$image' alt='Image of the animal.' class='animal-karton-img'>
    <table class='animal-karton-desc'>
        <tr>
            <td>Owner: </td><td>$user_name</td> 
        </tr>
        <tr>
            <td>Birth: </td><td>$birth</td>
        </tr>
        <tr>
            <td>Breed: </td><td>$breed</td>
        </tr>
    </table>";

if ($role == 'admin' || $role == 'doctor') {

    echo "<form action='index.php?op=form&for=new' method='POST' class='animal-karton-form'>
              <button class='animal-karton-button' type='submit' >New Operation</button>
          </form>";
}

if ($role != 'doctor') { ?>
    <form action="" id="animal_modify" method="GET">
        <table>
            <tr>
                <td><label for="name">Name: </label> </td>
                <td><input type="text" value="<?= $_POST['name'] ?>" id="name" name="name"></td>
            </tr>
            <tr>
                <td><label for="birth">Birth: </label> </td>
                <td><input type="text" value="<?= $_POST['birth'] ?>" id="birth" name="birth"></td>
            </tr>
            <tr>
                <td><label for="pic_upload">Image: </label> </td>
                <td><input type="file" id="pic_upload" name="pic_upload"></td>
            </tr>
            <tr>
                <input type="hidden" value="<?= $image ?>" id="picture" name="picture">
                <input type="hidden" value="<?= $petId ?>" id="pet_id" name="pet_id">
                <td><input type="submit" name="animal_modify" value="Update" id="submit" onclick="validateAnimalUpdate(event)"></td>
                <td><input type="submit" name="animal_delete" value="Delete" onclick="return confirm('Are you sure you want to delete?');"></td>
            </tr>
        </table>
    </form>

<?php }
echo "<h1 class='animal-karton-operations'>Operations</h1>
      <table class='animal-karton-data'>
          <tr>
              <th>Doctor</th>
              <th>Date</th>
              <th>Service</th>
              <th>Price</th>
              <th>Description</th>
          </tr>";

//Select medical cards (medical history)
$doctorName = ""; $dateOfOperation = ""; $serviceOfOperation = ""; $descOfOperation = ""; $priceOfOperation = ""; $i = 0;
$medicalcard = array();
$sql = "SELECT medicalcards.id, users.firstname, users.lastname, services.name, medicalcards.date, medicalcards.price, medicalcards.description FROM `medicalcards`
        INNER JOIN users ON users.id = medicalcards.doc_id
        INNER JOIN pets ON pets.id = medicalcards.pet_id
        INNER JOIN services On services.id = medicalcards.service_id
        WHERE medicalcards.pet_id = $petId";
$query = $pdo->prepare($sql);
$query->execute();
$results_medicalcards = $query->fetchAll(PDO::FETCH_ASSOC);

//Insert medical cards to object, objects are stored in one dimension array
foreach ($results_medicalcards as $row) {
    $medicalcardId = $row['id'];
    $docName = $row['firstname'] . " " . $row['lastname'];
    $serviceName = $row['name'] . "";
    $medicalcardDate = $row['date'];
    $medicalcardPrice = $row['price'];
    $medicalcardDesc = $row['description'];
    $medicalcard[$i] = new medicalcards($medicalcardId, $petId, $docName, $serviceName, $medicalcardDate, $medicalcardPrice, $medicalcardDesc);
    $i++;
}

//Get data from object and show it to the user
for($j = 0; $j < count($medicalcard); $j++){

    echo "<tr>
            <td>{$medicalcard[$j]->getDoctor()}</td>
            <td>{$medicalcard[$j]->getDate()}</td>
            <td>{$medicalcard[$j]->getService()}</td>
            <td>{$medicalcard[$j]->getPrice()}</td>
            <td>{$medicalcard[$j]->getDescription()}</td>
        </tr>";
}

echo "</table>
  </div>";
?>

<script src='script.js'></script>

<?php // Upload image (update animal) ?>
<script>
var image = document.getElementById("pic_upload");

pic_upload.onchange = function() {
    pic_upload.disabled = true;
    let file = this.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        submit.disabled = true;
        let form_data = new FormData();
        form_data.set('image', reader.result.replace(/^.*?base64,/, ''));
        form_data.set('key', '733317fbddeda8ff5bd4ec1d2ec0a28d');

        fetch('https://api.imgbb.com/1/upload', {
            method: 'POST',
            body: form_data
        })
        .then(response => response.json())
        .then(json => {
            submit.disabled = false;
            console.log(json);
            // nem csak url-t ad vissza hanem tobb adatot is, konzolban meg lehet oket nezni
            picture.value = json.data.url;
        }) 
    };
}

</script>