<h1 style="text-align: left">OUR TEAM</h1>
<div id="doctors">
    <?php
    //List all doctors (team members) for user

    require_once "db_config.php";

    global $pdo;

    //Select users with status = 'doctor'
    $name = ""; $department = ""; $email = ""; $phone = ""; $image = "";
    $sql = "SELECT id, image, firstname, lastname, department, email, phone FROM users WHERE status='doctor'";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    foreach ($results as $row) {
        $name = $row['firstname'] . " " . $row['lastname'];
        $department = $row['department'] . "";
        $email = $row['email'] . "";
        $phone = $row['phone'] . "";
        $image = $row['image'];
        $id = $row['id'];

        echo "<a href='index.php?op=doc&id=$id' class='card' > 
                <img src='$image' alt='Image of the doctor' class='docImg'>
                <div class='docTexts'>
                    <p class='name'>$name</p><hr>
                    <p class='department'>$department</p>
                    <p class='email'>$email</p>
                    <p class='phone'>$phone</p>
                </div>
            </a>";
    }
    ?>
</div>