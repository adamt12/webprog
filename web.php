<?php
//Manage login/register/forgotPassword requests
session_start();

require_once "db_config.php";
require "functions_def.php";

global $pdo;
global $actions;

$password = "";
$passwordConfirm = "";
$firstname = "";
$lastname = "";
$email = "";
$phoneNumber = "";
$action = "";

$referer = $_SERVER['HTTP_REFERER'];
$action = $_POST["action"];
if ($action != "" and in_array($action, $actions) and strpos($referer, SITE) !== false) {

    switch ($action) {

        case "login":

            if (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $email = mb_substr($pdo->quote(trim($_POST['email'])), 1, -1);
            }

            if (isset($_POST['password']) and strlen($_POST['password']) >= 8) {
                $password = mb_substr($pdo->quote(trim($_POST['password'])), 1, -1);
            }

            if (!empty($email) and !empty($password)) {
                $data = checkUserLogin($email, $password, "user");

                if ($data and is_int($data['id'])) {
                    $_SESSION['email'] = $email;
                    $_SESSION['id'] = $data['id'];
                    $_SESSION['role'] = $data['role'];
                    redirection('index.php?op=profile');
                } else {
                    redirection('login.php?l=1');
                }

            } else {
                redirection('login.php?l=1');
            }
            break;


        case "register" :

            if (isset($_POST['fname'])) {
                $firstname = mb_substr($pdo->quote(trim($_POST['fname'])), 1, -1);
            }

            if (isset($_POST['lname'])) {
                $lastname = mb_substr($pdo->quote(trim($_POST['lname'])), 1, -1);
            }

            if (isset($_POST['passwordReg']) and strlen($_POST['password']) >= 8) {
                $password = mb_substr($pdo->quote(trim($_POST['passwordReg'])), 1, -1);
            }

            if (isset($_POST['passwordConfirm']) and strlen($_POST['password']) >= 8) {
                $passwordConfirm = mb_substr($pdo->quote(trim($_POST['passwordConfirm'])), 1, -1);
            }

            if (isset($_POST['emailReg']) and filter_var($_POST['emailReg'], FILTER_VALIDATE_EMAIL)) {
                $email = mb_substr($pdo->quote(trim($_POST['emailReg'])), 1, -1);
            }

            if (isset($_POST['phone'])) {
                $phoneNumber = mb_substr($pdo->quote(trim($_POST['phone'])), 1, -1);
            }


            if (empty($firstname)) {
                redirection('login.php?r=4');
            }

            if (empty($lastname)) {
                redirection('login.php?r=4');
            }

            if (empty($password) or strlen($password) < 7) {
                redirection('login.php?r=9');
            }

            if (empty($passwordConfirm) or strlen($passwordConfirm) < 7) {
                redirection('login.php?r=9');
            }

            if ($password !== $passwordConfirm) {
                redirection('login.php?r=7');
            }

            if (empty($email) or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                redirection('login.php?r=8');
            }

            if (!existsUser($email)) {
                $token = createCode(40);
                $id_web = registerUser($password, $firstname, $lastname, $email, $phoneNumber, $token);

                if (sendData($email, $token, 'register')) {
                    redirection("login.php?r=3");
                } else {
                    redirection("login.php?r=10");
                }

            } else {
                redirection('login.php?r=2');
            }
            break;

        case "forget" :

            if (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $email = mb_substr($pdo->quote(trim($_POST['email'])), 1, -1);
            } else {
                redirection('login.php?r=1');
            }

            if (existsUser($email)) {
                $token = createCode(40);
                $id_web = updatePassword($email, $token);

                if ($id_web) {

                    if (sendData($email, $token, 'forgot')) {
                        redirection("login.php?r=3");
                    } else {
                        redirection("login.php?r=10");
                    }

                } else {
                    redirection("login.php?r=13");
                }
            } else {
                redirection("login.php?r=1");
            }

            break;

        default:
            redirection('index.php?l=0');
            break;
    }
} else {
    redirection('index.php?l=0');
}